# Create React App

- [Create React App](https://github.com/facebook/create-react-app)
- [Update on Async Rendering](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html)
- [ ] Wait for upcoming 16.3 where new lifecycle hooks `getDerivedStateFromProps` and `getSnapshotBeforeUpdate` will be available and try them out

## TypeScript

`create-react-app x --scripts-version=react-scripts-ts`

`tslint.json`:

```
{
    // …
    "rules": {
        
    }
}
```
